package com.hinzmann.craftifense.ui;

import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

public class MyButton extends TextButton {

    private final float percWidth;
    private final float percHeight;

    public MyButton(String text, TextButtonStyle tbs, float percWidth, float percHeight) {
        super(text, tbs);
        this.percWidth = percWidth;
        this.percHeight = percHeight;
    }

    public void resize(float screenWidth, float screenHeight) {
        final float btnWidth = percWidth * screenWidth;
        final float btnHeight = percHeight * screenHeight;

        setWidth(btnWidth);
        setHeight(btnHeight);
    }
}
