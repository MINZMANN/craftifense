package com.hinzmann.craftifense.ads;

/**
 * Created by Mirco on 26.10.2015.
 */
public interface AdHandler {

    void loadAd();

    void showAd();

    void hideAd();

    void pauseAd();

    void resumeAd();
}
