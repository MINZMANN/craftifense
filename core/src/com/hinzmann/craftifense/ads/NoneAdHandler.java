package com.hinzmann.craftifense.ads;

/**
 * Created by Mirco on 26.10.2015.
 */
public class NoneAdHandler implements AdHandler {

    @Override
    public void loadAd() {
    }

    @Override
    public void showAd() {

    }

    @Override
    public void hideAd() {
    }

    @Override
    public void pauseAd() {

    }

    @Override
    public void resumeAd() {

    }
}
