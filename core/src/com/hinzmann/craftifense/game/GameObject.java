package com.hinzmann.craftifense.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by mm on 27.10.2015.
 */
public class GameObject extends Actor {

    private Texture texture;

    public GameObject(Texture texture, float x, float y){
        this.texture = texture;
        setX(x);
        setY(y);
    }

    @Override
    public void draw(Batch batch, float parentAlpha){
        batch.draw(texture, getX(), getY());
    }

}

