package com.hinzmann.craftifense.game.towers;


import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.hinzmann.craftifense.game.GameObject;
import com.hinzmann.craftifense.game.towers.shootModules.ShootModule;
import com.hinzmann.craftifense.util.Optional;

import static com.hinzmann.craftifense.resources.ResourcePaths.TOWER_SOCKET;

public final class Tower extends GameObject {

    private Optional<ShootModule> shootModuleOpt;

    public Tower(AssetManager assets){
        super(assets.get(TOWER_SOCKET, Texture.class), 0, 0);
        setTouchable(Touchable.disabled);
        shootModuleOpt = Optional.empty();
    }

    public void setShootModuleOpt(ShootModule shootModule) {
        shootModuleOpt = Optional.of(shootModule);
        shootModuleOpt.get().setX(getX());
        shootModuleOpt.get().setY(getY());
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        if(shootModuleOpt.isPresent())
            shootModuleOpt.get().draw(batch, parentAlpha);
    }

    @Override
    public void act(float delta) {
        if(shootModuleOpt.isPresent())
            shootModuleOpt.get().act(delta);
    }
}
