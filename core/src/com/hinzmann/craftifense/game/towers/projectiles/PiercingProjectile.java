package com.hinzmann.craftifense.game.towers.projectiles;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.hinzmann.craftifense.resources.ResourcePaths;

/**
 * Created by Mirco on 01.11.2015.
 */
public class PiercingProjectile extends Projectile {

    public PiercingProjectile(AssetManager assets, float x, float y, Vector2 dir) {
        super(assets.get(ResourcePaths.PROJECTILE_PIERCING, Texture.class), x, y, dir);
    }

    @Override
    void move(float delta, Vector2 dir) {

    }
}
