package com.hinzmann.craftifense.game.towers.projectiles;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.hinzmann.craftifense.game.GameObject;

public abstract class Projectile extends GameObject {

    private final Vector2 dir;

    public Projectile(Texture texture, float x, float y, Vector2 dir) {
        super(texture, x, y);
        this.dir = dir;
    }

    @Override
    public void act(float delta) {
        move(delta, dir);
    }

    abstract void move(float delta, Vector2 dir);

}
