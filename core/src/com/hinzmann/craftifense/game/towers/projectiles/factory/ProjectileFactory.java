package com.hinzmann.craftifense.game.towers.projectiles.factory;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.hinzmann.craftifense.game.towers.projectiles.PiercingProjectile;
import com.hinzmann.craftifense.game.towers.projectiles.Projectile;
import com.hinzmann.craftifense.game.towers.projectiles.ProjectileType;

/**
 * Created by Mirco on 01.11.2015.
 */
public class ProjectileFactory {

    private final Stage stage;
    private final AssetManager assets;

    public ProjectileFactory(Stage stage, AssetManager assets) {
        this.stage = stage;
        this.assets = assets;
    }

    public void spawnProjectile(ProjectileType type, float x, float y, Vector2 dir) {
        Projectile projectile;
        switch (type) {
            case PIERCING:
                projectile = new PiercingProjectile(assets, x, y, dir);
                break;
            default:
                throw new IllegalArgumentException("The projectile type " + type + " is not handled in the factory.");
        }
        stage.addActor(projectile);
    }

}
