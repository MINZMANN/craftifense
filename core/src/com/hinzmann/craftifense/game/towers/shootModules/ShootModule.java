package com.hinzmann.craftifense.game.towers.shootModules;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.hinzmann.craftifense.game.GameObject;
import com.hinzmann.craftifense.game.towers.projectiles.ProjectileType;
import com.hinzmann.craftifense.game.towers.projectiles.factory.ProjectileFactory;

/**
 * Created by Mirco on 30.10.2015.
 */
public abstract class ShootModule extends GameObject {

    private final ProjectileFactory projectileFactory;
    private final ProjectileType projectileType;

    private final float secondsBetweenAttacks;
    private float secondsPassed;

    public ShootModule(Texture texture, ProjectileFactory projectileFactory, ProjectileType projectileType, float attacksPerSecond) {
        super(texture, 0, 0);
        this.projectileFactory = projectileFactory;
        this.projectileType = projectileType;

        secondsBetweenAttacks = 1f / attacksPerSecond;
    }

    @Override
    public void act(float delta) {
        secondsPassed += delta;
        if(secondsPassed >= secondsBetweenAttacks) {
            secondsPassed -= secondsBetweenAttacks;
            onAttackTimeReached();
        }
    }

    abstract void onAttackTimeReached();

    protected void shoot(Vector2 dir) {
        projectileFactory.spawnProjectile(projectileType, getX(), getY(), dir);
    }

}
