package com.hinzmann.craftifense.game.towers.shootModules;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.hinzmann.craftifense.game.towers.projectiles.ProjectileType;
import com.hinzmann.craftifense.game.towers.projectiles.factory.ProjectileFactory;

import static com.hinzmann.craftifense.resources.ResourcePaths.SHOOTMODULE_BASIC;

/**
 * Created by Mirco on 01.11.2015.
 */
public class BasicShootModule extends ShootModule {

    public BasicShootModule(AssetManager assets, ProjectileFactory projectileFactory, ProjectileType projectileType, float attacksPerSecond) {
        super(assets.get(SHOOTMODULE_BASIC, Texture.class), projectileFactory, projectileType, attacksPerSecond);
    }

    @Override
    void onAttackTimeReached() {
        Vector2 dir = new Vector2(1, 0);
        shoot(dir);
    }
}
