package com.hinzmann.craftifense.game.enemies;


import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;

import com.hinzmann.craftifense.game.GameObject;

import java.util.List;

/**
 * Created by mm on 25.10.2015.
 */
public class Enemie extends GameObject {

    private static final float WAYPOINT_EPSILON = 0.1f;

    private int speed;
    private int health;

    private final List<Vector2> waypoints;
    private int waypointsPassed;
    private Vector2 nextWaypoint;

    public Enemie(Texture texture, float x, float y, int speed, int health, List<Vector2> waypoints) {
        super(texture, x, y);
        this.speed = speed;
        this.health = health;

        this.waypoints = waypoints;
        nextWaypoint = waypoints.get(waypointsPassed);
    }

    @Override
    public void act(float delta) {
        if(arrivedAtNextWaypoint()) {
            waypointsPassed++;
            if(areNoWaypointsLeft()) {
                //enemy passed
            } else {
                nextWaypoint = waypoints.get(waypointsPassed);
            }
        }
        move(delta);
    }

    private void move(float delta) {
        float distX = nextWaypoint.x - getX();
        float distY = nextWaypoint.y - getY();
        Vector2 direction = new Vector2(distX, distY).nor();

        setX(getX() + direction.x * delta * speed);
        setY(getY() + direction.y * delta * speed);
    }

    private boolean arrivedAtNextWaypoint() {
        return nextWaypoint.epsilonEquals(getX(), getY(), WAYPOINT_EPSILON);
    }

    private boolean areNoWaypointsLeft() {
        return waypoints.size() == waypointsPassed;
    }
}






