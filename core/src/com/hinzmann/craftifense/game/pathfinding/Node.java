package com.hinzmann.craftifense.game.pathfinding;

/**
 * Created by Mirco on 27.10.2015.
 */
class Node implements Comparable<Node> {
    int x;
    int y;
    int depth;
    float cost;
    float heuristic;
    Node parent;

    public Node(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void setParent(Node parent) {
        depth = parent.depth + 1;
        this.parent = parent;
    }

    @Override
    public int compareTo(Node o) {
        float f = heuristic + cost;
        float of = o.heuristic + o.cost;

        if (f > of) {
            return 1;
        } else if (f < of) {
            return -1;
        } else {
            return 0;
        }
    }
}
