package com.hinzmann.craftifense.game.pathfinding;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Mirco on 27.10.2015.
 */
public class PathFinder {

    private final Passable map;

    private final Array<Node> open = new Array<>();
    private final Array<Node> closed = new Array<>();
    private final List<Vector2> path = new ArrayList<>();

    private final Node[][] nodes;

    private final int mapWidth;
    private final int mapHeight;

    public PathFinder(Passable map) {
        this.map = map;
        mapWidth = map.getGridWidth();
        mapHeight = map.getGridHeight();
        nodes = new Node[mapWidth][mapHeight];
        for (int x = 0; x < mapWidth; x++) {
            for (int y = 0; y < mapHeight; y++) {
                nodes[x][y] = new Node(x, y);
            }
        }
    }

    public boolean findPath(int startX, int startY, int targetX, int targetY) {
        Gdx.app.log("Pathfinder", "Finding path from " + startX + " " + startY + " to " + targetX + " " + targetY);

        path.clear();
        closed.clear();
        open.clear();
        open.add(nodes[startX][startY]);

        nodes[startX][startY].depth = 0;
        nodes[startX][startY].cost = 0f;
        nodes[targetX][targetY].parent = null;
        while (open.size > 0) {
            Node current = open.get(0);

            if (current == nodes[targetX][targetY]) {
                break;
            }

            open.removeValue(current, false);
            closed.add(current);

            for (int x = -1; x <= 1; x++) {
                for (int y = -1; y <= 1; y++) {
                    if (x == 0 && y == 0 || (Math.abs(x) + Math.abs(y) == 2))
                        continue;

                    int xp = current.x + x;
                    int yp = current.y + y;

                    if (isBlocked(xp, yp)) {
                        continue;
                    }

                    float nextStepCost = current.cost + 1;
                    Node neighbor = nodes[xp][yp];

                    if (nextStepCost < neighbor.cost) {
                        open.removeValue(neighbor, false);
                        closed.removeValue(neighbor, false);
                    }

                    if (!open.contains(neighbor, false)
                            && !closed.contains(neighbor, false)
                            && !isBlocked(x + current.x, current.y)
                            && !isBlocked(current.x, y + current.y)) {

                        neighbor.cost = nextStepCost;
                        neighbor.heuristic = getHeuristicCost(xp, yp, targetX, targetY);
                        neighbor.setParent(current);
                        open.add(neighbor);
                        open.sort();
                    }
                }
            }
        }


        if (nodes[targetX][targetY].parent == null) {
            Gdx.app.log("Pathfinder", "No path found..");
            return false;
        }

        Node target = nodes[targetX][targetY];
        while (nodes[startX][startY] != target) {
            path.add(new Vector2(target.x, target.y));
            target = target.parent;
        }

        Collections.reverse(path);

        Gdx.app.log("Pathfinder", "Path found!");
        return true;
    }

    public List<Vector2> getPath() {
        return Collections.unmodifiableList(path);
    }

    private float getHeuristicCost(int x, int y, int tx, int ty) {
        int dx = tx - x;
        int dy = ty - y;
        return (float) Math.sqrt(dx * dx + dy * dy);
    }

    private boolean isBlocked(int x, int y) {
        if (x < 0 || y < 0 || x >= mapWidth || y >= mapHeight) {
            return true;
        }

        return map.isBlocked(x, y);
    }

}
