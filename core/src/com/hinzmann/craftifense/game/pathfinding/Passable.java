package com.hinzmann.craftifense.game.pathfinding;

/**
 * Created by Mirco on 27.10.2015.
 */
public interface Passable {

    int getGridWidth();

    int getGridHeight();

    boolean isBlocked(int x, int y);

}
