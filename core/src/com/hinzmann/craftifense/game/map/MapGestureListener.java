package com.hinzmann.craftifense.game.map;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

public class MapGestureListener implements GestureDetector.GestureListener {

    private static final float BASE_ZOOM = 0.5f;

    private final Map map;
    private final OrthographicCamera camera;

    private float initialScale;

    public MapGestureListener(Map map, OrthographicCamera camera) {
        this.map = map;
        this.camera = camera;

        camera.zoom = BASE_ZOOM;
    }

    @Override
    public boolean touchDown(float x, float y, int pointer, int button) {
        initialScale = camera.zoom;
        return true;
    }

    @Override
    public boolean tap(float x, float y, int count, int button) {
        Vector3 screenCoords = new Vector3(x, y, 0);
        camera.unproject(screenCoords);
        if(map.inBounds(screenCoords.x, screenCoords.y)){
            map.select(screenCoords.x, screenCoords.y);
            return true;
        }
        return false;
    }

    @Override
    public boolean longPress(float x, float y) {
        return false;
    }

    @Override
    public boolean fling(float velocityX, float velocityY, int button) {
        return false;
    }

    @Override
    public boolean pan(float x, float y, float deltaX, float deltaY) {
        float moveX = -deltaX * camera.zoom;
        float moveY = deltaY * camera.zoom;
        camera.translate(moveX, moveY);

        //move camera position if out of mapRange
        correctOverlap();
        return true;
    }

    @Override
    public boolean panStop(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean zoom(float initialDistance, float distance) {
        camera.zoom = initialScale * (initialDistance / distance);

        final float viewWidth = camera.viewportWidth;
        final float viewHeight = camera.viewportHeight;

        //clamp zoom to map
        final float maxZoomX = map.getPixelWidth() / viewWidth;
        final float maxZoomY = map.getPixelHeight() / viewHeight;
        final float minZoom = 0.2f;
        final float maxZoom = Math.min(maxZoomX, maxZoomY);
        camera.zoom = MathUtils.clamp(camera.zoom, minZoom, maxZoom);

        correctOverlap();

        return true;
    }

    private void correctOverlap(){
        final float viewWidth = camera.viewportWidth;
        final float viewHeight = camera.viewportHeight;

        Vector3 cameraMinExtent = new Vector3(camera.position.x - (viewWidth / 2f) * camera.zoom, camera.position.y - (viewHeight / 2f) * camera.zoom, 0);
        Vector3 camMaxExtent = new Vector3(camera.position.x + ((viewWidth / 2f) * camera.zoom), camera.position.y + (viewHeight / 2f) * camera.zoom, 0);

        Vector3 minDist = new Vector3();
        minDist.set(map.getX(), map.getY(), 0);
        minDist.sub(cameraMinExtent);

        minDist.set(Math.max(0, minDist.x), Math.max(0, minDist.y), 0);

        Vector3 maxDist = new Vector3();
        maxDist.set(map.getX(), map.getY(), 0);
        maxDist.add(map.getPixelWidth(), map.getPixelHeight(), 0f);
        maxDist.sub(camMaxExtent);

        maxDist.set(Math.min(0, maxDist.x), Math.min(0, maxDist.y), 0);

        float xCorrection = maxDist.x + minDist.x;
        float yCorrection = maxDist.y + minDist.y;

        camera.translate(xCorrection, yCorrection);
    }

    @Override
    public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
        return false;
    }
}
