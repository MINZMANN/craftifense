package com.hinzmann.craftifense.game.map;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.hinzmann.craftifense.game.GameObject;
import com.hinzmann.craftifense.game.pathfinding.Passable;
import com.hinzmann.craftifense.game.towers.Tower;
import com.hinzmann.craftifense.util.Optional;

import static com.hinzmann.craftifense.resources.ResourcePaths.SELECT_MARKER;

public class Map extends Group implements Passable {

    private static final int LAYER_GROUND = 0;
    private static final int LAYER_BLOCKERS = 1;
    private static final int LAYER_PORTALS = 2;

    private final MapGestureListener inputProcessor;

    private final TiledMapRenderer renderer;
    private final AssetManager assets;

    private final int gridWidth;
    private final int gridHeight;

    private final int tileWidth;
    private final int tileHeight;

    private final MyCell[][] cells;

    private final GameObject selectMarker;
    private Optional<MyCell> selectedCellOpt;

    public Map(AssetManager assets, String mapPath, OrthographicCamera camera){
        this.assets = assets;
        TiledMap map = assets.get(mapPath, TiledMap.class);

        inputProcessor = new MapGestureListener(this, camera);
        renderer = new OrthogonalTiledMapRenderer(map);

        gridWidth = map.getProperties().get("width", Integer.class);
        gridHeight = map.getProperties().get("height", Integer.class);

        tileWidth = map.getProperties().get("tilewidth", Integer.class);
        tileHeight = map.getProperties().get("tileheight", Integer.class);

        cells = initGrid(map);

        selectedCellOpt = Optional.empty();
        selectMarker = new GameObject(assets.get(SELECT_MARKER, Texture.class), 0, 0);
    }

    private MyCell[][] initGrid(TiledMap map) {
        MyCell[][] cells = new MyCell[gridWidth][gridHeight];

        TiledMapTileLayer ground = (TiledMapTileLayer) map.getLayers().get(LAYER_GROUND);
        TiledMapTileLayer blockers = (TiledMapTileLayer) map.getLayers().get(LAYER_BLOCKERS);
        TiledMapTileLayer portals = (TiledMapTileLayer) map.getLayers().get(LAYER_PORTALS);

        for (int x = 0; x < cells.length; x++) {
            for (int y = 0; y < cells[0].length; y++) {
                boolean blocked = blockers.getCell(x, y) != null;

                MyCell myCell = new MyCell(blocked, x * tileWidth, y * tileHeight);
                cells[x][y] = myCell;
            }
        }

        return cells;
    }

    public void render(OrthographicCamera camera) {
        renderer.setView(camera);
        renderer.render();
    }

    public void select(float x, float y) {
        if(selectedCellOpt.isPresent())
            deselect();

        int gridX = (int) (x / tileWidth);
        int gridY = (int) (y / tileHeight);

        MyCell cell = cells[gridX][gridY];
        if(!cell.isBlocked()) {
            addActor(cell);
            cell.addActor(selectMarker);
            selectedCellOpt = Optional.of(cell);
        } else if(cell.towerOpt.isPresent()) {
            //TODO select tower
        }
    }

    public void deselect() {
        MyCell cell = selectedCellOpt.get();
        cell.removeActor(selectMarker);
        removeActor(cell);
        selectedCellOpt = Optional.empty();
    }

    public void placeTower() {
        if(!selectedCellOpt.isPresent())
            throw new IllegalStateException("You can't place a tower if you did not select a cell first!");

        MyCell selectedCell = selectedCellOpt.get();

        selectedCell.placeTower(new Tower(assets));
        addActor(selectedCell);
    }

    @Override
    public int getGridWidth() {
        return gridWidth;
    }

    @Override
    public int getGridHeight() {
        return gridHeight;
    }

    @Override
    public boolean isBlocked(int x, int y) {
        return cells[x][y].isBlocked();
    }

    public int getPixelWidth() {
        return gridWidth * tileWidth;
    }

    public int getPixelHeight() {
        return gridHeight * tileHeight;
    }

    public MapGestureListener getInputProcessor() {
        return inputProcessor;
    }

    public boolean inBounds(float x, float y) {
        boolean inXBounds = x >= 0 && x < gridWidth * tileWidth;
        boolean inYBounds = y >= 0 && y < gridHeight * tileHeight;
        return inXBounds && inYBounds;
    }

    private class MyCell extends Group {

        private Optional<Tower> towerOpt;
        private final boolean blocked;

        public MyCell(boolean blocked, int x, int y) {
            setX(x);
            setY(y);
            this.blocked = blocked;
            towerOpt = Optional.empty();
        }

        boolean isBlocked(){
            return blocked || towerOpt.isPresent();
        }

        void placeTower(Tower tower) {
            if(towerOpt.isPresent())
                throw new IllegalStateException("There already is a tower on this cell!");
            if(blocked)
                throw new IllegalStateException("You can't place a tower on a blocked cell!");
            towerOpt = Optional.of(tower);
            addActor(tower);
        }
    }
}
