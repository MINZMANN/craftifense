package com.hinzmann.craftifense;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.hinzmann.craftifense.ads.AdHandler;
import com.hinzmann.craftifense.ads.NoneAdHandler;
import com.hinzmann.craftifense.screens.SplashScreen;

public class Craftifense extends Game {

    private AdHandler adHandler;

    public Craftifense() {
        this(new NoneAdHandler());
    }

    public Craftifense(AdHandler adHandler) {
        this.adHandler = adHandler;
    }

	@Override
	public void create () {
        setScreen(new SplashScreen(this, adHandler));
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void render() {
        Gdx.gl.glClear(Gdx.gl20.GL_COLOR_BUFFER_BIT);
        super.render();
    }

}
