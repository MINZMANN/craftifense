package com.hinzmann.craftifense.resources;

/**
 * Created by Mirco on 31.10.2015.
 */
public final class ResourcePaths {

    public static final String MAP_WASTELAND = "maps/wasteland.tmx";
    public static final String TOWER_SOCKET = "towers/socket.png";

    public static final String SHOOTMODULE_BASIC = "towers/shootmodules/basic.png";

    public static final String PROJECTILE_PIERCING = "towers/projectiles/piercing.png";
    public static final String SELECT_MARKER = "marking/selectMarker.png";

    private ResourcePaths() {
    }
}
