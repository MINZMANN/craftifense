package com.hinzmann.craftifense.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.hinzmann.craftifense.game.map.Map;

import static com.hinzmann.craftifense.resources.ResourcePaths.MAP_WASTELAND;

public class IngameScreen implements Screen {

    private final Game game;
    private final AssetManager assets;

    private final OrthographicCamera camera;
    private final Stage stage;

    private final Map map;

    public IngameScreen(Game game, AssetManager assets) {
        this.game = game;
        this.assets = assets;

        camera = initCamera();

        stage = initStage(camera);
        map = initMap(assets, camera);
        stage.addActor(map);
        Gdx.input.setInputProcessor(new GestureDetector(map.getInputProcessor()));
    }

    private OrthographicCamera initCamera() {
        Gdx.app.log("Ingame-Init", "Camera");
        OrthographicCamera camera = new OrthographicCamera();
        float height = Gdx.graphics.getHeight();
        float width = Gdx.graphics.getWidth();
        camera.setToOrtho(false, width, height);
        camera.update();
        return camera;
    }

    private Stage initStage(Camera camera) {
        Gdx.app.log("Ingame-Init", "Stage");
        Stage stage = new Stage();
        stage.getViewport().setCamera(camera);
        Gdx.input.setInputProcessor(stage);
        return stage;
    }

    private Map initMap(AssetManager assets, OrthographicCamera camera) {
        Gdx.app.log("Ingame-Init", "Map");
        return new Map(assets, MAP_WASTELAND, camera);
    }

    @Override
    public void show() {
        Gdx.app.log("Ingame-Screen", "Show");
    }

    @Override
    public void render(float delta) {
        map.render(camera);
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        dispose();
    }

    @Override
    public void dispose() {
        assets.dispose();
    }
}
