package com.hinzmann.craftifense.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.hinzmann.craftifense.ads.AdHandler;

/**
 * Created by Mirco on 26.10.2015.
 */
public class SplashScreen implements Screen {

    private static final int SECONDS_SHOWING = 3;

    private final Game game;
    private final AdHandler adHandler;

    private SpriteBatch batch;
    private BitmapFont font;

    private float currentShowTime;

    public SplashScreen(Game game, AdHandler adHandler) {
        this.game = game;
        this.adHandler = adHandler;

        batch = new SpriteBatch();
        font = new BitmapFont();
    }

    @Override
    public void show() {
        Gdx.app.log("Splash-Screen", "Show");

        adHandler.loadAd();
        adHandler.hideAd();
    }

    @Override
    public void render(float delta) {
        batch.begin();
        font.draw(batch, "Splashscreen", Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2);
        batch.end();

        currentShowTime += delta;
        if(currentShowTime > SECONDS_SHOWING)
            game.setScreen(new MainMenuScreen(game, adHandler));
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {
        adHandler.pauseAd();
    }

    @Override
    public void resume() {
        adHandler.resumeAd();
    }

    @Override
    public void hide() {
    }

    @Override
    public void dispose() {
        batch.dispose();
    }
}
