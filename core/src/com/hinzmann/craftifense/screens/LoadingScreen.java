package com.hinzmann.craftifense.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;

import static com.hinzmann.craftifense.resources.ResourcePaths.*;

/**
 * Created by Mirco on 25.10.2015.
 */
public class LoadingScreen implements Screen {

    private Game game;

    private AssetManager assets;

    public LoadingScreen(Game game) {
        this.game = game;
    }

    @Override
    public void show() {
        Gdx.app.log("Loading-Screen", "Show");

        assets = new AssetManager();
        // loading assets for this screen
        assets.finishLoading();

        // load all assets for ingame
        assets.setLoader(TiledMap.class, new TmxMapLoader());
        assets.load(MAP_WASTELAND, TiledMap.class);
        assets.load(TOWER_SOCKET, Texture.class);
        assets.load(SHOOTMODULE_BASIC, Texture.class);
        assets.load(PROJECTILE_PIERCING, Texture.class);
        assets.load(SELECT_MARKER, Texture.class);
    }

    @Override
    public void render(float delta) {
        if(assets.update()) {
            game.setScreen(new IngameScreen(game, assets));
        }
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void dispose() {
    }
}
