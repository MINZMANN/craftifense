package com.hinzmann.craftifense.screens;


import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.hinzmann.craftifense.ads.AdHandler;
import com.hinzmann.craftifense.ui.MyButton;

import java.util.ArrayList;
import java.util.List;

public class MainMenuScreen implements Screen {

    private static final float PADDING_TOP_PERC = 0.1f;
    private static final float BTN_WIDTH_PERC = 0.3f;
    private static final float BTN_HEIGHT_PERC = 0.2f;
    private static final float BTN_PADDING = 0.05f;

    private final Game game;
    private final AdHandler adHandler;

    private Stage stage;

    private List<MyButton> buttons;

    public MainMenuScreen(Game game, AdHandler adHandler) {
        this.game = game;
        this.adHandler = adHandler;

        buttons = new ArrayList<>();
    }

    @Override
    public void show() {
        Gdx.app.log("MainMenu-Screen", "Show");

        stage = new Stage();
        Gdx.input.setInputProcessor(stage);
        initButtons(stage);
        adHandler.showAd();
    }

    private void initButtons(Stage stage) {
        buttons.clear();

        TextButton.TextButtonStyle tbs = createStyle();

        MyButton btnStart = new MyButton("Start", tbs, BTN_WIDTH_PERC, BTN_HEIGHT_PERC);

        btnStart.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(new LoadingScreen(game));
                adHandler.hideAd();
            }
        });

        MyButton btnShowAd = new MyButton("Show Ad", tbs, BTN_WIDTH_PERC, BTN_HEIGHT_PERC);

        btnShowAd.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                adHandler.showAd();
            }
        });

        MyButton btnHideAd = new MyButton("Hide Ad", tbs, BTN_WIDTH_PERC, BTN_HEIGHT_PERC);
        btnHideAd.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                adHandler.hideAd();
            }
        });

        buttons.add(btnStart);
        buttons.add(btnShowAd);
        buttons.add(btnHideAd);

        for (MyButton btn : buttons) {
            stage.addActor(btn);
        }
    }

    private TextButton.TextButtonStyle createStyle() {
        TextureRegionDrawable trd = new TextureRegionDrawable(new TextureRegion(new Texture("ui/button.png")));

        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("ui/font/grobold.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 50;
        parameter.borderStraight = true;
        parameter.borderWidth = 10;
        parameter.borderColor = new Color(0.3f, 0.173f, 0.047f, 1);
        BitmapFont font = generator.generateFont(parameter);
        generator.dispose();

        return new TextButton.TextButtonStyle(trd, trd, trd, font);
    }

    @Override
    public void render(float delta) {
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
        final int btnWidth = (int) (width * BTN_WIDTH_PERC);
        final int btnHeight = (int) (height * BTN_HEIGHT_PERC);

        final int btnPosLeft = width / 2 - btnWidth / 2;
        final int btnPosTop = (int) (height - (height * PADDING_TOP_PERC) - btnHeight);

        int offsetY = 0;
        for (MyButton btn : buttons) {
            btn.resize(width, height);
            btn.setX(btnPosLeft);
            btn.setY(btnPosTop - offsetY);

            offsetY += btnHeight + (BTN_PADDING * height);
        }
    }

    @Override
    public void pause() {
        adHandler.pauseAd();
    }

    @Override
    public void resume() {
        adHandler.resumeAd();
    }

    @Override
    public void hide() {
    }

    @Override
    public void dispose() {
        adHandler.hideAd();
        stage.dispose();
    }
}
