package com.hinzmann.craftifense.game.pathfinding;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Mirco on 27.10.2015.
 */
public class PathFinderTest {

    @BeforeClass
    public static void initGdx() {
        Gdx.app = Mockito.mock(Application.class);
    }

    @Test
    public void testPathfinding() {
        PathFinder pf = new PathFinder(initMap());

        pf.findPath(0, 0, 0, 2);

        List<Vector2> path = pf.getPath();

        assertTrue(path != null);
        assertEquals(new Vector2(1, 0), path.get(0));
        assertEquals(new Vector2(2, 0), path.get(1));
        assertEquals(new Vector2(2, 1), path.get(2));
        assertEquals(new Vector2(2, 2), path.get(3));
        assertEquals(new Vector2(1, 2), path.get(4));
        assertEquals(new Vector2(0, 2), path.get(5));
    }

    private Passable initMap() {
        return new Passable() {
            @Override
            public boolean isBlocked(int x, int y) {
                if(x == 1 && y == 1)
                    return true;
                if(x == 0 && y == 1)
                    return true;
                return false;
            }

            @Override
            public int getGridWidth() {
                return 3;
            }

            @Override
            public int getGridHeight() {
                return 3;
            }
        };
    }
}
