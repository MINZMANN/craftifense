package com.hinzmann.craftifense.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.hinzmann.craftifense.Craftifense;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.width = 1196;
        config.height = 768;
		new LwjglApplication(new Craftifense(), config);
	}
}
