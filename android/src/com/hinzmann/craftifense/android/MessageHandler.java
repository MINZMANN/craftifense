package com.hinzmann.craftifense.android;

import android.os.Handler;
import android.os.Message;
import android.view.View;

import com.badlogic.gdx.Gdx;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.hinzmann.craftifense.ads.AdHandler;

/**
 * Created by Mirco on 26.10.2015.
 */
public class MessageHandler extends Handler implements AdHandler {

    private static final int LOAD_AD = 0;
    private static final int SHOW_AD = 1;
    private static final int HIDE_AD = 2;
    private static final int PAUSE_AD = 3;
    private static final int RESUME_AD = 4;

    private AdView adView;

    public MessageHandler(AdView adView) {
        this.adView = adView;
    }

    @Override
    public void loadAd() {
        sendEmptyMessage(LOAD_AD);
    }

    @Override
    public void showAd() {sendEmptyMessage(SHOW_AD);}

    @Override
    public void hideAd() {
        sendEmptyMessage(HIDE_AD);
    }

    @Override
    public void pauseAd() {
        sendEmptyMessage(PAUSE_AD);
    }

    @Override
    public void resumeAd() {
        sendEmptyMessage(RESUME_AD);
    }

    @Override
    public void handleMessage(Message msg) {
        switch (msg.what) {
            case LOAD_AD:
                Gdx.app.log("Ads", "Load");
                adView.loadAd(new AdRequest.Builder().build());
                break;
            case SHOW_AD:
                Gdx.app.log("Ads", "Show");
                adView.setVisibility(View.VISIBLE);
                break;
            case HIDE_AD:
                Gdx.app.log("Ads", "Hide");
                adView.setVisibility(View.GONE);
                break;
            case PAUSE_AD:
                Gdx.app.log("Ads", "Pause");
                adView.pause();
                break;
            case RESUME_AD:
                Gdx.app.log("Ads", "Resume");
                adView.resume();
                break;
            default:
                throw new IllegalArgumentException("Unkown message type: " + msg.what);
        }
    }
}
